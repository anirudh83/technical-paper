# DOM :
DOM is the Document Object Model which is used to representing the documents as sequential structures. It shows how Browser sees the web pages. By using the DOM, the developer can alter the web pages using Javascript. DOM contains one root element and it has branches like head and body and body contains its own branches that's why it also call the DOM tree. According to the Document Object Model (DOM), every HTML tag is an object. For example, document.body is the object representing the body tag.
         	![alt text](https://www.w3schools.com/whatis/img_htmltree.gif)
# DOM Node tree: 
-  **Node tree:** Document, Text, DocumentFragment, Element,  ProcessingInstruction, DocumentType, and Comment objects (simply called nodes) participate in a tree, simply named the node tree.
-  **Document tree:** The document element of a document is the element whose parent is that document if it exists, and null otherwise.
-  **Shadow tree:**  A shadow tree is never alone. It is always attached to another node tree through its host. The node tree of a shadow root's host is sometimes referred to as the light tree.

         	
# Fetch HTML Elements:
- **By ID:**   `document.getElementById` method is used for fetch element which have id. If Id is not present then it returns null.
**Example:**
    > `var myElement = document.getElementById("user-name");`
- **By  Tag Name:**   `document.getElementsByTagName` method is used for fetch element by tag like p,a,li,ul,etc. It return NodeList which is array of DOM element.
**Example:**
    > `var x = document.getElementsByTagName("p");`
- **By  Class Name:**   `document.getElementsByClassName` method is used for fetch element by class name. It also return NodeList.
**Example:**
    > `var x = document.getElementsByClassName("intro");`
- **By  CSS Selector:**   `document.querySelector` and `document.querySelectorAll` these two method use for CSS Selector.
`document.querySelector` returns NodeList and `document.querySelectorAll` returns the first Element within the document that matches the specified selector
**Example:**
    > `var x = document.getElementsByClassName("intro");`
## Reference:
- [Image](https://www.w3schools.com/whatis/img_htmltree.gif)
- [Node tree](https://dom.spec.whatwg.org/#document-trees)
- [DOM](https://www.w3schools.com/js/js_htmldom.asp)
- [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)
